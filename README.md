# Apprendre à utiliser Git - Purple Giraffe
1. Ce cours d'initiation à Git utilise l'IDE Tower en exemple *(c'est un outil graphique dédié payant mais avec 30 jours gratuits > coupon de réduction avec purple giraffe).*
2. Le dépot Local (Repositorie) est notre fichier cours-git
3. Le dépôt distant -> utilisation de GitLab (se créer son compte etc.)


## Exemples issus du cours
> Ce dépôt est utilisé dans le cours purple Giraffe pour présenter et expliquer l'utilisation de Git.
> 
> Note : La branche principale nommée 'master' se nomme par  défaut 'main' dans GitLab, j'ai donc fusionné master dans main. 


# Sommaire
* [Installation](Install.md)
* [Travailler au quotidien](Daily.md)
